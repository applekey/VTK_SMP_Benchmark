#-------------------------------------------------------PUT SOME NOTES HERE ------------------------------------------------------------
#-------------------------------------------------------PUT SOME NOTES HERE ------------------------------------------------------------
#-------------------------------------------------------PUT SOME NOTES HERE ------------------------------------------------------------


#-------------------------------------------------------GENERAL CONFIGURE SETTINGS HERE ------------------------------------------------

#This is where the
TEST_PROGRAM_PATH="..//..//..//bin//SMPThreadedImageAlgorithm"

#AVERAGE NUMBER OF ITERATIONS
NUMBER_OF_TIMES_TEST_RUN=3


#Enable/Disable Oprofile
O_PROFILE_ENABLE=0

#Set Oprofile Events
O_PERF_EVENTS="--event=CPU_CLK_UNHALTED:100000,l2_rqsts:200000:0x80"

#-------------------------------------------------------TEST SPECIFIC CONFIGURATION SETTINGS------------------------------------------------
#NUMBER OF WORKSIZE SAMPLES TAKEN
NUMBER_OF_SAMPLES=9

# Range Start and End is divided by 10

WORK_SIZES_TEST1="1024"
BLOCK_RANGE_START1=1
BLOCK_RANGE_END1=750

WORK_SIZES_TEST2="512"
BLOCK_RANGE_START2=1
BLOCK_RANGE_END2=750
KERNEL_SIZE=5

WORK_SIZES_TEST3="512"
BLOCK_RANGE_START3=1
BLOCK_RANGE_END3=750
STENCIL_RADIUS=100

WORK_SIZES_TEST4="512"
BLOCK_RANGE_START4=1
BLOCK_RANGE_END4=750

WORK_SIZES_TEST5="1024"
BLOCK_RANGE_START5=1
BLOCK_RANGE_END5=750
STENCIL_RADIUS=30

#------------------------------------------------------------------------------------------------------------------------------------------
#SPECIFY WHICH TEST TO RUN
TESTS_TO_RUN="1 2 3 4 5"

#RUN DESCRIPTION
RUN_DESCRIPTION="$(date '+%d-%m-%Y_%H-%M-%S')"

#MAXIUM NUMBER OF PROCESSORS
processorCountSizes="1 2 4 8 12 24 28 32 36"

#------------------------------------------------------------------------------------------------------------------------------------------

#make results directory
RESULTS_DIRECTORY="results"
mkdir -p "$RESULTS_DIRECTORY"

#set output csv file
OUTPUT_CSV_FILE="$RESULTS_DIRECTORY/NewSplitTests.csv"

# Set Rootfolder to current date
if [ $O_PROFILE_ENABLE -eq 1 ]; then
  dt=Test_$(date '+%d-%m-%Y_%H-%M-%S');
  mkdir "$RESULTS_DIRECTORY/$dt"
fi

#PUT DateTime into csv
echo $RUN_DESCRIPTION >> "$OUTPUT_CSV_FILE"

echo "Number of Iterations Run,TestCase Number,SMP-ON/OFF,BLOCKING-ON/OFF,NumberOfProcessors,# blocks,WorkSize,Average ExecutionTime,Standard Deviation,Additional Parms" >> "$OUTPUT_CSV_FILE"

#---------------------TEST CASE 1-------------------------
# Test SMP overhead versus old multi-threader
# This test does a very numerical operation filter imagecast
if [[ ${TESTS_TO_RUN[*]} =~ 1 ]]; then
  TEST_NUMBER=1

	echo "TestCase1" >> "$OUTPUT_CSV_FILE"

	LOCAL_DIRECTORY="$RESULTS_DIRECTORY/$dt/TestCase1"
	if [ $O_PROFILE_ENABLE -eq 1 ]; then
		mkdir $LOCAL_DIRECTORY
	fi

	for  workSize in `echo $WORK_SIZES_TEST1`
	do
		echo "---,SMP-OFF,---,---,---" >> "$OUTPUT_CSV_FILE"
		./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER false false "999" "-1" $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE"

		for  processorCount in `echo $processorCountSizes`
		do
			echo " ,$processorCount PROCESSORS" >> "$OUTPUT_CSV_FILE"

			echo "---,SMP-ON BLOCKING-ON,---,---,---" >> "$OUTPUT_CSV_FILE"
			for (( blockSize = $BLOCK_RANGE_START1; blockSize <= $BLOCK_RANGE_END1; blockSize+=(($BLOCK_RANGE_END1-$BLOCK_RANGE_START1)/$NUMBER_OF_SAMPLES) ))
			do
			    ./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER true true $processorCount $blockSize $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY"  "$OUTPUT_CSV_FILE"
			done

			echo "---,SMP-ON BLOCKING-OFF,---,---,---" >> "$OUTPUT_CSV_FILE"
			for (( blockSize = $BLOCK_RANGE_START1; blockSize <= $BLOCK_RANGE_END1; blockSize+=(($BLOCK_RANGE_END1-$BLOCK_RANGE_START1)/$NUMBER_OF_SAMPLES) ))
			do
			    ./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER true false $processorCount $blockSize $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY"  "$OUTPUT_CSV_FILE"
			done
		done
	done
fi


#---------------------TEST CASE 2-------------------------
# Test median filter.

if [[ ${TESTS_TO_RUN[*]} =~ 2 ]]; then
	TEST_NUMBER=2

	echo "TestCase2" >> "$OUTPUT_CSV_FILE"

	LOCAL_DIRECTORY="$RESULTS_DIRECTORY/$dt/TestCase2"
	if [ $O_PROFILE_ENABLE -eq 1 ]; then
		mkdir $LOCAL_DIRECTORY
	fi

	ADDITIONAL_DATA=$KERNEL_SIZE

	for  workSize in `echo $WORK_SIZES_TEST2`
	do
		echo "---,SMP-OFF,---,---,---" >> "$OUTPUT_CSV_FILE"
		./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER false false "999" "-1" $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE" "$ADDITIONAL_DATA"

		for  processorCount in `echo $processorCountSizes`
		do
			echo " ,$processorCount PROCESSORS" >> "$OUTPUT_CSV_FILE"

			echo "---,SMP-ON BLOCKING-ON,---,---,---" >> "$OUTPUT_CSV_FILE"

			for (( blockSize=$BLOCK_RANGE_START2; blockSize <= $BLOCK_RANGE_END2; blockSize+=(($BLOCK_RANGE_END2-$BLOCK_RANGE_START2)/$NUMBER_OF_SAMPLES) ))
			do
			./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER true true $processorCount $blockSize $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE" "$ADDITIONAL_DATA"
			done

			echo "---,SMP-ON BLOCKING-OFF,---,---,---" >> "$OUTPUT_CSV_FILE"

			for (( blockSize=$BLOCK_RANGE_START2; blockSize <= $BLOCK_RANGE_END2; blockSize+=(($BLOCK_RANGE_END2-$BLOCK_RANGE_START2)/$NUMBER_OF_SAMPLES) ))
			do
			./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER true false $processorCount $blockSize $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE" "$ADDITIONAL_DATA"
			done
		done
	done
fi


#---------------------TEST CASE 3-------------------------
# Test Imagereslice 90 degree image rotation
# This test among other things mainly tests for
# Data Access pattern again by doing a 90 degree rotation

if [[ ${TESTS_TO_RUN[*]} =~ 3 ]]; then
	TEST_NUMBER=3
	echo "TestCase 3" >> "$OUTPUT_CSV_FILE"

	LOCAL_DIRECTORY="$RESULTS_DIRECTORY/$dt/TestCase3"
	if [ $O_PROFILE_ENABLE -eq 1 ]; then
		mkdir $LOCAL_DIRECTORY
	fi

	ADDITIONAL_DATA="$STENCIL_RADIUS"

	for  workSize in `echo $WORK_SIZES_TEST3`
	do
		echo "---,SMP-OFF Stencil-OFF,---,---,---" >> "$OUTPUT_CSV_FILE"
		./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER false false "999" "-1" $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE"
		echo "---,SMP-OFF Stencil-ON,---,---,---" >> "$OUTPUT_CSV_FILE"
		./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER false false "999" "-1" $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE" "$ADDITIONAL_DATA"

		for  processorCount in `echo $processorCountSizes`
		do
			echo " ,$processorCount PROCESSORS" >> "$OUTPUT_CSV_FILE"

			echo "---,Stencil OFF,---,---,---" >> "$OUTPUT_CSV_FILE"

			echo "---,SMP-ON BLOCKING-ON,---,---,---" >> "$OUTPUT_CSV_FILE"
			for (( blockSize = $BLOCK_RANGE_START3; blockSize <= $BLOCK_RANGE_END3; blockSize+=(($BLOCK_RANGE_END3-$BLOCK_RANGE_START3)/$NUMBER_OF_SAMPLES) ))
			do
			./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER true true $processorCount $blockSize $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE"
			done

			echo "---,SMP-ON BLOCKING-OFF,---,---,---" >> "$OUTPUT_CSV_FILE"
			for (( blockSize = $BLOCK_RANGE_START3; blockSize <= $BLOCK_RANGE_END3; blockSize+=(($BLOCK_RANGE_END3-$BLOCK_RANGE_START3)/$NUMBER_OF_SAMPLES) ))
			do
			./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER true false $processorCount $blockSize $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE"
			done

			echo "---,Stencil ON,---,---,---" >> "$OUTPUT_CSV_FILE"

			echo "---,SMP-ON BLOCKING-ON,---,---,---" >> "$OUTPUT_CSV_FILE"
			for (( blockSize = $BLOCK_RANGE_START3; blockSize <= $BLOCK_RANGE_END3; blockSize+=(($BLOCK_RANGE_END3-$BLOCK_RANGE_START3)/$NUMBER_OF_SAMPLES) ))
			do
			./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER true true $processorCount $blockSize $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE" "$ADDITIONAL_DATA"
			done

			echo "---,SMP-ON BLOCKING-OFF,---,---,---" >> "$OUTPUT_CSV_FILE"
			for (( blockSize = $BLOCK_RANGE_START3; blockSize <= $BLOCK_RANGE_END3; blockSize+=(($BLOCK_RANGE_END3-$BLOCK_RANGE_START3)/$NUMBER_OF_SAMPLES) ))
			do
			./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER true false $processorCount $blockSize $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE" "$ADDITIONAL_DATA"
			done


		done
	done
fi


#---------------------TEST CASE 4-------------------------
# Test Imagereslice 90 degree image rotation
# This test among other things mainly tests for
# Data Access pattern again by doing a 90 degree rotation
if [[ ${TESTS_TO_RUN[*]} =~ 4 ]]; then
	TEST_NUMBER=4
	echo "TestCase 4" >> "$OUTPUT_CSV_FILE"

	LOCAL_DIRECTORY="$RESULTS_DIRECTORY/$dt/TestCase4"
	if [ $O_PROFILE_ENABLE -eq 1 ]; then
		mkdir $LOCAL_DIRECTORY
	fi

	# Run test base class

	for  workSize in `echo $WORK_SIZES_TEST4`
	do
		echo "---,SMP-OFF ---,---,---" >> "$OUTPUT_CSV_FILE"
	  ./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER false false "999" "-1" $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE"


		echo "---,SMP-ON BLOCKING-ON,---,---,---" >> "$OUTPUT_CSV_FILE"
		for  processorCount in `echo $processorCountSizes`
		do
			echo " ,$processorCount PROCESSORS" >> "$OUTPUT_CSV_FILE"

			echo "---,SMP-ON BLOCKING-ON,---,---,---" >> "$OUTPUT_CSV_FILE"

			for (( blockSize = $BLOCK_RANGE_START4; blockSize <= $BLOCK_RANGE_END4; blockSize+=(($BLOCK_RANGE_END4-$BLOCK_RANGE_START4)/$NUMBER_OF_SAMPLES) ))
			do
			./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER true true $processorCount $blockSize $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE" "$ADDITIONAL_DATA"
			done

			echo "---,SMP-ON BLOCKING-OFF,---,---,---" >> "$OUTPUT_CSV_FILE"

			for (( blockSize = $BLOCK_RANGE_START4; blockSize <= $BLOCK_RANGE_END4; blockSize+=(($BLOCK_RANGE_END4-$BLOCK_RANGE_START4)/$NUMBER_OF_SAMPLES) ))
			do
			./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER true false $processorCount $blockSize $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE" "$ADDITIONAL_DATA"
			done
		done
	done
fi

#---------------------TEST CASE 5-------------------------
# Test vtkImagingStatistics

if [[ ${TESTS_TO_RUN[*]} =~ 5 ]]; then
	TEST_NUMBER=5
	echo "TestCase 5" >> "$OUTPUT_CSV_FILE"

	LOCAL_DIRECTORY="$RESULTS_DIRECTORY/$dt/TestCase5"
	if [ $O_PROFILE_ENABLE -eq 1 ]; then
		mkdir $LOCAL_DIRECTORY
	fi

	ADDITIONAL_DATA="$STENCIL_RADIUS"

	for  workSize in `echo $WORK_SIZES_TEST5`
	do
		echo "---,SMP-OFF Stencil-OFF,---,---,---" >> "$OUTPUT_CSV_FILE"
		./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER false false "999" "-1" $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE"

		echo "---,SMP-OFF Stencil-ON,---,---,---" >> "$OUTPUT_CSV_FILE"
		./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER false false "999" "-1" $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE" "$ADDITIONAL_DATA"

		for  processorCount in `echo $processorCountSizes`
		do
			echo " ,$processorCount PROCESSORS" >> "$OUTPUT_CSV_FILE"

			echo "---,Stencil OFF,---,---,---" >> "$OUTPUT_CSV_FILE"

			echo "---,SMP-ON BLOCKING-ON,---,---,---" >> "$OUTPUT_CSV_FILE"
			for (( blockSize = $BLOCK_RANGE_START5; blockSize <= $BLOCK_RANGE_END5; blockSize+=(($BLOCK_RANGE_END5-$BLOCK_RANGE_START5)/$NUMBER_OF_SAMPLES) ))
			do
			./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER true true $processorCount $blockSize $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE"
			done

			echo "---,SMP-ON BLOCKING-OFF,---,---,---" >> "$OUTPUT_CSV_FILE"
			for (( blockSize = $BLOCK_RANGE_START5; blockSize <= $BLOCK_RANGE_END5; blockSize+=(($BLOCK_RANGE_END5-$BLOCK_RANGE_START5)/$NUMBER_OF_SAMPLES) ))
			do
			./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER true false $processorCount $blockSize $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE"
			done

			echo "---,Stencil ON,---,---,---" >> "$OUTPUT_CSV_FILE"

			echo "---,SMP-ON BLOCKING-ON,---,---,---" >> "$OUTPUT_CSV_FILE"
			for (( blockSize = $BLOCK_RANGE_START5; blockSize <= $BLOCK_RANGE_END5; blockSize+=(($BLOCK_RANGE_END5-$BLOCK_RANGE_START5)/$NUMBER_OF_SAMPLES) ))
			do
			./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER true true $processorCount $blockSize $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE" "$ADDITIONAL_DATA"
			done

			echo "---,SMP-ON BLOCKING-OFF,---,---,---" >> "$OUTPUT_CSV_FILE"
			for (( blockSize = $BLOCK_RANGE_START5; blockSize <= $BLOCK_RANGE_END5; blockSize+=(($BLOCK_RANGE_END5-$BLOCK_RANGE_START5)/$NUMBER_OF_SAMPLES) ))
			do
			./SMPBenchMarking.sh $TEST_PROGRAM_PATH $O_PROFILE_ENABLE $NUMBER_OF_TIMES_TEST_RUN $TEST_NUMBER true false $processorCount $blockSize $workSize "$O_PERF_EVENTS" "$LOCAL_DIRECTORY" "$OUTPUT_CSV_FILE" "$ADDITIONAL_DATA"
			done

		done
	done
fi
